#!/usr/bin/make -Rf

# PREREQUISITES
#  Bowtie (http://bowtie-bio.sourceforge.net/index.shtml)
#  Samtools (module load samtools, http://samtools.sourceforge.net/)
#  R (module load R)
#  R packages : getopts, Rargs
#  Sickle (https://github.com/ucdavis-bioinformatics/sickle/) - paired-end fastq quality filtering
#  fastq_removeAdaptersPE.sh
#  cutadapt
#  rev_comp.pl

# This pipeline is for quality control and alignment of paired-end datasets.
# It assumes that both 'fastq.gz' files have the same name suffixed by _1 and _2 before the extension and are in the same folder.
# It should be called by specifying the 'forwardReads' parameter as the name of the file suffixed by '_1'.
# Sample name (if not specified as argument) and reverse reads file name will be inferred from the filename of forward reads.



##############
# CHECK ARGS #
##############

# The pipeline uses the name of the first fastq file for defining the workflow but relies on both files for data processing

ifndef forwardReads
$(error missing argument forwardReads)
endif



###############
# DEFINE ARGS #
###############

# Common requested files
REF_GEN_EBW?=/sc/orga/projects/vanbah01a/reference-databases/homo-sapiens/UCSC/hg19/Sequence/BowtieIndex/genome

# General parameters
name?=$(notdir $(forwardReads:_1.fastq.gz=))
np            ?=8
awkt          :=awk -F '\t' -v OFS='\t'


# Step specific parameters
qc_qualityThreshold  ?=20
qc_minLength         ?=20
qc_adapterForward    ?=AATGATACGGCGACCACCGAGATCTACACTCTTTCCCTACACGACGCTCTTCCGATCT
qc_adapterReverse    ?=AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC

pasha_elongationSize ?=NA
pasha_threshold      ?=10000000
pasha_binSize        ?=50
pasha_nbSizeRanges   ?=NA
pasha_nbCores        ?=3


# Subfolders definition
folderFastq          :=$(dir $(forwardReads))
folderQualTrimming 	 ?=1_fastq_qualTrimming/
folderBams           ?=2_bamFiles/
folderReports        ?=reports/
subfolderWigs        :=wigsPasha_PE/



#########
# RULES #
#########

all: qc map wigs


qc: $(folderFastq)$(folderQualTrimming) \
    $(folderFastq)$(folderQualTrimming)$(name)_1_qcTrim_noAdapters.fastq


map: $(folderFastq)$(folderBams) \
     $(folderFastq)$(folderBams)$(name).bam

wigs: $(folderFastq)$(folderBams)$(subfolderWigs)

report :  $(folderFastq)$(folderReports) \


.SECONDARY: # do not remove ANY intermediate file



#####################
# CREATE SUBFOLDERS #
#####################

# create the directories, if they don't exist
$(folderFastq)$(folderQualTrimming) $(folderFastq)$(folderBams) $(folderFastq)$(folderReports):
	@mkdir -p $@



######
# QC #
######

# Decompress gz files, remove low quality reads, and attribute experiment name to output files
$(folderFastq)$(folderQualTrimming)$(name)_1_qcTrim.fastq: $(forwardReads)
	sickle pe -f $< -r $(<:_1.fastq.gz=_2.fastq.gz) -o $@ -p $(@:_1_qcTrim.fastq=_2_qcTrim.fastq) -t sanger -q $(qc_qualityThreshold) -l $(qc_minLength) -x -s $(folderFastq)$(folderQualTrimming)$(name)_SINGLES_qcTrim.fastq > $(folderFastq)$(folderQualTrimming)$(name)_qcTrim.log 2>&1;

# Remove eventual remaining adapters from paired reads
$(folderFastq)$(folderQualTrimming)$(name)_1_qcTrim_noAdapters.fastq: $(folderFastq)$(folderQualTrimming)$(name)_1_qcTrim.fastq
	fastq_removeAdaptersPE.sh $< $(<:_1_qcTrim.fastq=_2_qcTrim.fastq) $(qc_adapterForward) $(qc_adapterReverse) > $(folderFastq)$(folderQualTrimming)$(name)_qcTrim_noAdapters.log 2>&1



#######
# MAP #
#######

$(folderFastq)$(folderBams)$(name).bam: $(folderFastq)$(folderQualTrimming)$(name)_1_qcTrim_noAdapters.fastq
	bowtie -v 2 -X 10000 -k 1 -m 1 -p $(np) -S $(REF_GEN_EBW) -1 $< -2 $(<:_1_qcTrim_noAdapters.fastq=_2_qcTrim_noAdapters.fastq) 2> $(@:.bam=.txt) | samtools view -S -b -@ 2 -o $@ - 




########################
# WIG TRACK GENERATION #
########################

$(folderFastq)$(folderBams)$(subfolderWigs): $(folderFastq)$(folderBams)$(name).bam
	Pasha.R --expName $(name) --inputFile $< --inputType BAM --threshold $(pasha_threshold) --bins $(pasha_binSize) --nbSizeRanges $(pasha_nbSizeRanges) --pairedEnds TRUE --elongationSize $(pasha_elongationSize) --midPoint FALSE --CPU $(pasha_nbCores) --resultSubFolder $(subfolderWigs:_PE/=)


