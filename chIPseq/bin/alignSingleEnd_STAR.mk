#!/usr/bin/make -Rf

# PREREQUISITES
#  Bowtie (http://bowtie-bio.sourceforge.net/index.shtml)
#  Samtools (module load samtools, http://samtools.sourceforge.net/)
#  R (module load R)
#  R packages : getopts, Rargs
#  Sickle (https://github.com/ucdavis-bioinformatics/sickle/) - paired-end fastq quality filtering

###############
# DEFINE ARGS #
###############


projectsFolder:=/sc/orga/projects/
#projectsFolder:=/sc/gaus/projects/


# Common requested files
GENOMEREF_STAR?=$(projectsFolder)/vanbah01a/reference-databases/homo-sapiens/UCSC/hg19/Sequence/StarIndex

# General parameters
np            ?=8
awkt          :=awk -F '\t' -v OFS='\t'


# Step specific parameters
qc_qualityThreshold  :=20
qc_minLength         :=20
qc_adapterSequence   ?=GATCGGAAGAGCACACGTCTGAACTCCAGTCAC
map_nbMaxMismatches  ?=5
pasha_elongationSize ?=NA
pasha_threshold      ?=10000000
pasha_binSize        ?=50
pasha_nbCores        ?=3

# Subfolders definition (WIG one has to finish by '_SE' as imposed by Pasha)
folderFastq          :=./
folderQualTrimming 	 :=1_fastq_qualTrimming/
folderBams           :=2_bamFiles/
folderReports        :=reports/
subfolderWigs        :=wigsPasha_SE/


##############
# CHECK ARGS #
##############

ifndef forward
$(error missing argument forward)
else
name?=$(notdir $(forward:.fastq.gz=))
endif


#########
# RULES #
#########

all: qc map wigs


qc:   $(folderQualTrimming) \
      $(folderQualTrimming)$(name)_noAdapters_qcTrim.fastq


map:  $(folderBams) \
      $(folderBams)$(name).bam

wigs: $(folderBams)$(subfolderWigs)

report :  $(folderReports) \


.SECONDARY: # do not remove ANY intermediate file


#####################
# CREATE SUBFOLDERS #
#####################

# create the directories, if they don't exist
$(folderQualTrimming) $(folderBams) $(folderReports):
	@mkdir -p $@



####################
# ADAPTERS REMOVAL #
####################

$(folderQualTrimming)$(name)_noAdapters.fastq: $(forward)
	cutadapt -a $(qc_adapterSequence) -m $(qc_minLength) -o $@ $< > $(folderQualTrimming)$(name)_1_cutadapt.txt 2>&1;



######
# QC #
######

$(folderQualTrimming)$(name)_noAdapters_qcTrim.fastq: $(folderQualTrimming)$(name)_noAdapters.fastq
	sickle se -f $< -o $(folderQualTrimming)$(notdir $(<:.fastq=_qcTrim.fastq)) -t sanger -q $(qc_qualityThreshold) -l $(qc_minLength) -x > $(folderQualTrimming)$(name)_2_qcTrimming.txt 2>&1;



#######
# MAP #
#######

$(folderBams)$(name).bam: $(folderQualTrimming)$(name)_noAdapters_qcTrim.fastq
	STAR --runThreadN $(np) --genomeDir $(GENOMEREF_STAR) --readFilesIn $< --outFilterMismatchNmax $(map_nbMaxMismatches) --outFilterMultimapNmax 1 --outFileNamePrefix $(folderBams)$(name). --outStd SAM  | samtools view -S -b -o $@ -



########################
# WIG TRACK GENERATION #
########################

$(folderBams)$(subfolderWigs): $(folderBams)$(name).bam
	Pasha.R --expName $(name) --inputFile $< --inputType BAM --threshold $(pasha_threshold) --bins $(pasha_binSize) --pairedEnds FALSE --elongationSize $(pasha_elongationSize) --midPoint FALSE --CPU $(pasha_nbCores) --resultSubFolder $(subfolderWigs:_SE/=)

