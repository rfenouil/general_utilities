#!/usr/bin/perl

# Gives the reverse complement of a string to stdout
#
################################################################################


# Get the input sequence
my $originalSeq=$ARGV[0];

# Compute the reverse DNA string
my $revSeq = reverse ($originalSeq);

# Compute the complement DNA string
$revSeq =~ tr/ATGCatgc/TACGtacg/;

print($revSeq);

