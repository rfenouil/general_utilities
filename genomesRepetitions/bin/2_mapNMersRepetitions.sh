#!/bin/sh
 
header="
#######################
#
# This scripts reads the nMers files generated by previous script 
# '1_extractNMersFromFasta.sh' and tries to infer the 'repetitiveness'
# of each region of the genome
#
#######################
#
# Usage : $0 n nbCores memSize
#
# Arguments :
#          n    size of the nMers
#    nbCores    number of cores used for nMers extraction and sorting algorithms
#    memSize    memory that will be allocated for sorting (in GB)
#
# Dependences :
#    GNU parallel
#    coreutils >=8.12 (parallel sort)
#
# Author : R. Fenouil - 02/04/2014
#
"


##### Define parameters


configFile="$(dirname $0)/0_parameters.sh"

if [ ! -f "$configFile" ]
then
    echo "\n\nERROR : Cannot find configuration file '$configFile'"
    exit 1
fi

. "$configFile"


##### Process arguments


echo -e "\n##### $(date)\n"

if [ 3 -ne "$#" ]
then
    echo "$header \n\nERROR : Bad number of arguments..."
    exit 1
fi

# assigning arguments to variables
n=$1
nbCores=$2
memSizeG=$3


if [ 0 -ge "$n" ]
then
    echo "$header \n\nERROR : The nMer length must be strictly positive"
    exit 1
fi

if [ 0 -ge "$nbCores" ]
then
    echo "$header \n\nERROR : The number of cores must be strictly positive"
    exit 1
fi

if [ 0 -ge "$memSizeG" ]
then
    echo "$header \n\nERROR : The memory assignment (in GB) must be strictly positive"
    exit 1
fi


##### Sorting the nMers


echo -e "\n##### $(date)\n"

# get the nMers filenames (for all the chromosomes/fasta file)
echo "Getting the files corresponding to ${n}mers that will be processed :"
#nMersFileNames=$(find ${WD}${nmersFilesFolder} -type f -name "*_${n}mers.txt.gz" -exec readlink -m {} \; | grep -E chr"M|Y") # TODO : REMOVE THE GREP AFTER TESTS !!!
nMersFileNames=$(find ${WD}${nmersFilesFolder} -type f -name "*_${n}mers.txt.gz" -exec readlink -m {} \;)
echo -e "Found : \n$(parallel basename {} ::: $nMersFileNames)"

# merge them (note that there is no quotes on the files given to pigz, otherwise it's considered as a unique filename)
nMersMergedFileName="${WD}${nmersFilesFolder}merged_${n}mers.txt"
echo ""
echo "Merging the ${n}mers from all compressed files -> $(basename $nMersMergedFileName)..."
pigz -p "$nbCores" -dc $nMersFileNames > "$nMersMergedFileName" 

# sort by sequence
echo ""
echo "Sorting the ${n}mers -> $(basename $nMersMergedFileName)..."
sort -k 3,3 -f -T "${WD}${tmpSortFolder}" -S "$memSizeG"G --parallel="$nbCores" "$nMersMergedFileName" -o "$nMersMergedFileName"


##### Counting the nMers


echo -e "\n##### $(date)\n"

duplicateMersFileName="${WD}${duplicateMersFilesFolder}duplicated_${n}mers.txt"
echo "Counting the duplicated ${n}mers -> $(basename $duplicateMersFileName)..."

#uniq -dc -f 1 "$nMersMergedFileName" > "$duplicateMersFileName" 
cut -d ' ' -f 3 "$nMersMergedFileName" | uniq -dci > "$duplicateMersFileName" 

##### Joining the files to report the duplication scores on the coordinates (using the sequence as reference : 3rd column in sorted nMersMergedFileName and 2nd in duplicateMersFileName)


echo -e "\n##### $(date)\n"

duplicateScoresFileName="${WD}${duplicateScoresFilesFolder}duplicatedScores_${n}mers.txt"
echo "Reporting the ${n}mers duplication scores to coordinates -> $(basename $duplicateScoresFileName)..."

join -o 1.1 1.2 2.1 -1 3 -2 2 "$nMersMergedFileName" "$duplicateMersFileName" > "$duplicateScoresFileName"

# Free some space !!!
rm "$nMersMergedFileName"
rm "$duplicateMersFileName"


##### Splitting the merged file in separate chromosomes files


echo -e "\n##### $(date)\n"

# starting from here the unique file will be split by feature (chromosomes), consequently we can only construct a "base" filename which will be used for every split
chromosomeScoresFileNameBase="${WD}${duplicateScoresFilesFolder}splitScores_${n}mers_"
echo "Splitting the ${n}mers scores by feature -> $(basename $chromosomeScoresFileNameBase)..."

# split all features in different files using the featureName contained in the first column (TODO : check that the parallel writing doesn't affect the result files consistency)
cat "$duplicateScoresFileName" | parallel -j "$nbCores" --pipe "awk -v baseName=$chromosomeScoresFileNameBase '{print \$2\" \"\$3 >> baseName\$1}'"

echo ""
echo "Sorting files by coordinates :"
# get the filenames generated in prevous step
chromosomeScoresFileNames=$(find "$(dirname $chromosomeScoresFileNameBase)" -type f -name "$(basename $chromosomeScoresFileNameBase)*" -exec readlink -m {} \;)
for fileNameToSort in $chromosomeScoresFileNames
do
	echo "$(basename $fileNameToSort)..."
	sort -k1n,1 -T "${WD}${tmpSortFolder}" -S "$memSizeG"G  --parallel="$nbCores" "$fileNameToSort" -o "$fileNameToSort"
done

# Free some space !!!
rm "$duplicateScoresFileName"


##### Transforming the scores at each duplicated position to a wig variable step format with basal score at 1 (no repetition) for other regions


echo -e "\n##### $(date)\n"

wigFileNameBase="${WD}${wigFilesFolder}RepetitionScores_${n}mers_"
echo "Writing the ${n}mers repetition scores WIG (variable steps) files -> $(basename $wigFileNameBase)..."

# extract the features names from filenames generated in previous step (by substracting the 'base' filename to the final ones)
previousFileNames=$(parallel -j "$nbCores" -k basename {} ::: "$chromosomeScoresFileNames")
previousBaseFileName=$(basename "$chromosomeScoresFileNameBase")
featuresNames=$(parallel -k -j "$nbCores" "a={}; echo \${a#$previousBaseFileName}" ::: "$previousFileNames")

# writing the headers
parallel "echo \"variableStep chrom={}\" > \"${wigFileNameBase}{}.wig\"" ::: "$featuresNames"

# converting scores to wig variable step format
parallel -j "$nbCores" "awk 'BEGIN{previousIndex=0;previousValue=\"\"} {if (\$1!=(previousIndex+1)) {print previousIndex+1\" 1\";print \$0} else if (\$2!=previousValue) print \$0}  {previousIndex=\$1;previousValue=\$2;} END{print previousIndex+1\" 1\"}' \"${chromosomeScoresFileNameBase}{}\" >> \"${wigFileNameBase}{}.wig\"" ::: "$featuresNames"

# Free some space !!!
parallel rm {} ::: "$chromosomeScoresFileNames"


#####


echo -e "\n##### $(date)\n"

exit 0


#####

