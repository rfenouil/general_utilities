#!/bin/sh
 
header="
#######################
#
# This scripts just defines the common parameters for other scripts
#
#######################
#
# Usage : Sourced by other scripts
#
# Arguments : -
#    
# Dependences : -
#
# Author : R. Fenouil - 02/04/2014
#
"


##### Define parameters


# working directory
WD="/sc/orga/projects/vanbah01a/influenza/cap-snatch/5prime-end-seq/tests_Romain/unicitySequencesTSS/"

# local folder where to store tmp files
tmpSortFolder="chromosomes/tmpSort/"

# where to store the results
flatFilesFolder="chromosomes/1_flatFiles/"
nmersFilesFolder="chromosomes/2_nMers/"
duplicateMersFilesFolder="chromosomes/3_duplicateMers/"
duplicateScoresFilesFolder="chromosomes/4_duplicateScores/"
wigFilesFolder="chromosomes/5_wigs/"
