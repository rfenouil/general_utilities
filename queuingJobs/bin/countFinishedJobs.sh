#!/bin/sh
 
header="
#######################
#
# Takes a set of queue job IDs and tells how many output file(s) could be found for this list of jobs
# NOTE : it does not check if the jobs are/were real IDs, extravagant job IDs will be counted as running jobs
#
#######################
#
# Usage : $0 'jobs IDs'
#
# Arguments :
#    jobs IDs : a space separated list of jobs IDs 
#
# Dependences : -
#
# Author : R. Fenouil - 04/14/2014
#
"


##### Check arguments


if [ 1 -gt $# ]
then
	echo -e "$header \n\nERROR : jobs IDs list must contain at least one job ID"
	exit 1
fi


##### Define parameters


jobOutputFolder="$HOME/lsf-output/"
jobOutputSuffix=".OU"


##### Check for the output files


jobsListRegularExpression=^$(echo $@ | tr -s ' ' | sed "s/ /$jobOutputSuffix\|^/g")$jobOutputSuffix

# return the number of job IDs that could not be found in the output folder
echo $(ls -1 "$jobOutputFolder" | grep -E "$jobsListRegularExpression" | wc -l)


#####


exit 0


#####
