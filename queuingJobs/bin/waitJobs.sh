#!/bin/sh
 
header="
#######################
#
# Takes a set of queue job IDs and waits for the output to pop-up in the output directory before unblocking
# NOTE : it does not check if the jobs are real, extravagant job IDs will be counted as running and cause the loop to never end
#
#######################
#
# Usage : $0 'jobs IDs'
#
# Arguments :
#    jobs IDs : a space separated list of jobs IDs 
#
# Dependences : 
#    'countFinishedJobs.sh' must be in the same folder as this script
#
# Author : R. Fenouil - 04/14/2014
#
"


##### Check arguments


if [ 1 -gt $# ]
then
	echo -e "$header \n\nERROR : jobs IDs list must contain at least one job ID"
	exit 1
fi


##### Define parameters


# tells how many seconds to wait between each loop iteration 
secondsIncrements=2

# path to the script that tells how many output were found for the specified job IDs
countFinishedJobs_script="$(dirname $0)/countFinishedJobs.sh"


##### Loop until all the output files were found


echo "WaitJobs : Waiting for jobs outputs : $(echo $@ | tr -s ' ')..."

nbSecs=0

while [ $# -ne $($countFinishedJobs_script $@) ]
do
	#echo "sleeping"
	sleep "$secondsIncrements";
	nbSecs=$((nbSecs+secondsIncrements))
done

echo "WaitJobs : All jobs outputs found after $(date -u -d @$nbSecs +"%H hour(s) %M minute(s) %S second(s)")"


#####


exit 0


#####
