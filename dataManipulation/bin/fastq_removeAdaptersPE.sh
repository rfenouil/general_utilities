#!/bin/sh

header="
#######################
#
# This script uses cutadapt to remove adapters from paired-ends fastq files
# It manages the generation and use of temporary fastq files
# It does search for adapters AND reverse complement of adapters
# 
# Requires : cutadapt, rev_comp.pl
#
#######################
#
# Usage : $(basename $0) file1 file2 adapter5 adapter3
#
# Arguments :
#    file    The full path to the fastq file(will be used to generate temp files too)
#    adapter 5 prime and 3 prime adapter sequences
#
# Author : R. Fenouil - 16/06/2014
#
"


if [ 4 -ne "$#" ]
then
    echo "$header \n\nERROR : Bad number of arguments..."
    exit 1
fi


file1=$1
file2=$2

adapter5=$3
adapter3=$4


if [ ! -f "$file1" ]
then
    echo "$file1 not found..."
    exit 1
fi

if [ ! -f "$file2" ]
then
    echo "$file2 not found..."
    exit 1
fi

#if file --mime-type "$file1" | grep -q gzip$; then	
#else	
#fi

folder="$(dirname $file1)/"

#echo "test $file1 --->  $(echo $file1 | sed 's/\.fastq/trimmed.fastq/')"
#echo "test $file2 --->  $(echo $file2 | sed 's/\.fastq/trimmed.fastq/')"


# cut from the first file and keep track of removed reads in paired file
echo "Removing 5 prime adapters from $file1"
cutadapt -f fastq -m 15 -e 0.15 -O 6 -a $adapter5 -a $(rev_comp.pl $adapter5) --paired-output ${folder}tmp2.fastq -o ${folder}tmp1.fastq $file1 $file2 | tee ${folder}removeAdapter5P.log

echo "Removing 3 prime adapters from $file2"
cutadapt -f fastq -m 15 -e 0.15 -O 6 -a $adapter3 -a $(rev_comp.pl $adapter3) --paired-output $(echo $file1 | sed 's/\.fastq/_noAdapters.fastq/') -o $(echo $file2 | sed 's/\.fastq/_noAdapters.fastq/') ${folder}tmp2.fastq ${folder}tmp1.fastq | tee ${folder}removeAdapter3P.log

echo "Deleting temporary files"
rm ${folder}tmp1.fastq ${folder}tmp2.fastq


