#!/bin/sh
 
header="
#######################
#
# This scripts takes a fastq file gives a subsample of 'n/2' sequences from top and 'n/2' from bottom, or randomly selected in the file
# The output is directly sent to stdout
# NOTE : using and odd 'n' number might results in an approximative number of sequences because the results of n/2 are truncated
# NOTE : random is very slow, one should not exceed 100k lines
#
#######################
#
# Usage : $0 fastaFile n random
#
# Arguments :
#    fastqFile    input fastq file
#            n    number of sequences to extract (see NOTE)
#       random    either 'true' or 'false', tells if the sequence selection must be random or from top/bottom 
#
# Dependences :
#    GNU parallel
#
# Author : R. Fenouil - 02/15/2014
#
"


##### Define parameters


# number of lines in file for each sequence
fastqSequenceLines=4; # ID, sequence, ID, quality score


##### Process arguments


#echo -e "\n##### $(date)\n"

if [ 3 -ne "$#" ]
then
    echo "$header \n\nERROR : Bad number of arguments..."
    exit 1
fi

# assigning arguments to variables
fastqFile=$1
n=$2
random=$3

#echo "fastq file : $fastqFile"
#echo "n : $n"

if [ ! -f "$fastqFile" ]
then
    echo "$header \n\nERROR : Cannot find fastq file"
    exit 1
fi

if [ 0 -ge "$n" ]
then
    echo "$header \n\nERROR : The nMers minimum size length must be strictly positive"
    exit 1
fi

if [ "$random" != "true" ] && [ "$random" != "false" ]  
then
    echo "$header \n\nERROR : The 'random' argument must be 'true' or 'false'"
    exit 1
fi


##### extract values to stout 


if  [ "$random" = "true" ]
then
	nbSeqInFastq=$(( ( $(wc -l < "$fastqFile") ) /fastqSequenceLines));
	
	# generates 'n' number between 1 an nbSeqInFastq (uses 32 bits (4x8) seed which mean that it cannot generate higher numers)
	#randomLines=$(seq "$n"| parallel -n0 "echo \$(((\$(od -An -N4 -i /dev/urandom | sed 's/-//')%$nbSeqInFastq)+1))")
	#randomLines=$(i=0;while [ "$i" -lt "$n" ]; do echo $((($(od -An -N4 -i /dev/urandom | sed 's/-//')%$nbSeqInFastq)+1)); i=$((i+1)); done)
	#echo "$randomLines" | parallel --pipe parallel "tail -n {} \"$fastqFile\" | head -n \"$fastqSequenceLines\""
	
	i=0;
	while [ "$i" -lt "$n" ]; 
	do 
		echo $i
		tail -n $((($(od -An -N4 -i /dev/urandom | sed 's/-//')%$nbSeqInFastq)+1)) "$fastqFile" | head -n "$fastqSequenceLines"
		i=$((i+1))
	done
	
else
	head -n $(((n/2)*fastqSequenceLines)) "$fastqFile"
	tail -n $(((n/2)*fastqSequenceLines)) "$fastqFile"
fi

exit 0