#!/usr/bin/env Rscript
#######################
#
# Wrapper script for average profile functions.
# It generates IOFs files (or read them if already saved on disk) then plot average profiles.
# It is possible to specify a text file of annotations that will be compared to the rest of annotations available in the IOFs. In this case, two curves will be plotted for each sample/IOF.
# A specific normalization process allows to rescale curves based on the max TSS value or the average value in gene body (50%->75%) from reference list of annotations (the ones not in list).
#
#######################
#
#
# Dependences :
#    library  Rargs - parsing command line arguments
#    library  RFutils
#
# Author : R. Fenouil - 05/11/2015
#


library(Rargs);
library(parallel);
library(rtracklayer); # defines several functions for loading annotations as IRanges objects

### Parameters from command line using Rargs ###

# Parameters format definition
paramsDefinition=list();

# mandatory (MUST BE SPECIFIED BY USER, DEFAULT VALUES ARE SPECIFIED FOR INFORMATION)
# -- input
paramsDefinition[["--scoreFiles|-W"]]=list(variableName="scoreFiles", numeric=F, mandatory=T, description="Path to file(s) containing genomic enrichment scores (WIG variable step format).", default="./test.wig");
paramsDefinition[["--annotationFile|-A"]]=list(variableName="annotationFile", numeric=F, mandatory=T, description="Path to the annotation (BED, GFF, see option '--loadAnnotationsFunction|-l') file(s) used as coordinate references for profiles.", default="./genesAnnotations.bed");

# non-mandatory (default value applied)
# -- output
paramsDefinition[["--folderOutput|-o"]]=list(variableName="folderOutput", numeric=F, mandatory=F, description="Path where the figure(s) will be written to.", default="./figures");
paramsDefinition[["--folderIOF|-i"]]=list(variableName="folderIOF", numeric=F, mandatory=F, description="Path where the intermediary object files (IOF) will be stored to or read from.", default="./IOFs");
paramsDefinition[["--outputFormat|-f"]]=list(variableName="outputFormat", numeric=F, mandatory=F, description="A value in 'pdf' or 'png' which defines the format of figures files.", default="pdf");
paramsDefinition[["--namesExperiments|-n"]]=list(variableName="namesExperiments", numeric=F, mandatory=F, description="Names to be used for reference to each '--scoreFiles|-W' argument (one value per score file). 'NULL' to use score base filenames (without extension). All special characters ( .|/\\) will be replaced by underscore '_' character.", default="NULL");
paramsDefinition[["--outputPrefix|-P"]]=list(variableName="outputPrefix", numeric=F, mandatory=F, description="Character string prefix to be added to output filenames.", default="");
# -- computation
paramsDefinition[["--extendRegionFigure|-e"]]=list(variableName="extendRegionFigure", numeric=T, mandatory=F, description="Size of the region (in number of bases) around reference annotations coordinates to be included in the figures (must be smaller or equal to '--extendRegionObject|-E').", default=2000);
paramsDefinition[["--extendRegionObject|-E"]]=list(variableName="extendRegionObject", numeric=T, mandatory=F, description="Size of the region (in number of bases) around each side of annotations from which the algorithm will retrieve the genomic enrichment scores (WIG files scores) and store them in the intermediary objects file (IOF).", default=10000);
paramsDefinition[["--rescaleData|-r"]]=list(variableName="rescaleData", numeric=F, mandatory=F, description="Defines if scores loaded from WIG file(s) should be rescaled based on the average coverage in each file.", default=TRUE, postConversion=as.logical);
paramsDefinition[["--saveIOF|-s"]]=list(variableName="saveIOF", numeric=F, mandatory=F, description="Defines if intermediary object files (IOF) containing scores around specified annotations should be saved after computations (can save a HUGE amount of time for further replotting).", default=TRUE, postConversion=as.logical);
paramsDefinition[["--useIOF|-u"]]=list(variableName="useIOF", numeric=F, mandatory=F, description="Defines if precomputed intermediary object files (IOF) should be used if found. Can save a lot of time on computations.", default=TRUE, postConversion=as.logical);
# -- plotting
paramsDefinition[["--mergeMultiProfiles|-m"]]=list(variableName="mergeMultiProfiles", numeric=F, mandatory=F, description="Defines if all experiments/samples should be plotted on the same panels. Ignored if a single score file is provided. If a list of annotations is provided for comparison with the other ones in the IOF, controls if the two sets should be on the same graph or not, see '--plotIndividuals' for sample separation.", default=TRUE, postConversion=as.logical);
paramsDefinition[["--nbPoints|-p"]]=list(variableName="nbPoints", numeric=T, mandatory=F, description="Number of points to be used for plotting each curve (interpolation).", default=1000);
paramsDefinition[["--ratioInsideExtend|-I"]]=list(variableName="ratioInsideExtend", numeric=T, mandatory=F, description="For composite profile, defines the number of points that should be dedicated for the inside part of the annotations as a ratio over the side extensions ('--extendRegionFigure|-e').", default=4);
paramsDefinition[["--figuresHeight|-h"]]=list(variableName="figuresHeight", numeric=T, mandatory=F, description="The vertical size of the figure. NOTE : this value is defined in inches or pixels for PDF and PNG output format respectively !", default=10);
paramsDefinition[["--figuresRatio|-w"]]=list(variableName="figuresRatio", numeric=T, mandatory=F, description="A numeric value defining the aspect ratio (height/width) of figures.", default=4/3);
paramsDefinition[["--composite|-c"]]=list(variableName="composite", numeric=F, mandatory=F, description="Defines if a panel containing the composite profile should be plotted.", default=TRUE, postConversion=as.logical);
paramsDefinition[["--fivePrime|-5"]]=list(variableName="fivePrime", numeric=F, mandatory=F, description="Defines if a panel containing the 5 prime centered profile should be plotted.", default=TRUE, postConversion=as.logical);
paramsDefinition[["--threePrime|-3"]]=list(variableName="threePrime", numeric=F, mandatory=F, description="Defines if a panel containing the 3 prime centered profile should be plotted.", default=TRUE, postConversion=as.logical);
paramsDefinition[["--scalingAll|-a"]]=list(variableName="scalingAll", numeric=F, mandatory=F, description="Defines if the figures panels (composite, 5p, 3p) should all be scaled the same or if the scale should be based on each panel content.", default=TRUE, postConversion=as.logical);
paramsDefinition[["--colors|-C"]]=list(variableName="colors", numeric=F, mandatory=F, description="Defines the color associated to each sample (recycled if necessary). A 'NULL' value attributes a different color to each sample (using rainbow functions).", default="NULL");
paramsDefinition[["--smoothingSpan|-S"]]=list(variableName="smoothingSpan", numeric=T, mandatory=F, description="Defines the number of points to be used for smoothing profiles. NULL to ignore smoothing", default="NULL");

# -- other
paramsDefinition[["--loadAnnotationsFunction|-l"]]=list(variableName="loadAnnotationsFunction", numeric=F, mandatory=F, description="Name of the R function used to read annotation file(s), has to return an IRanges object.", default="import.bed", postConversion=get);
paramsDefinition[["--nbCores|-N"]]=list(variableName="nbCores", numeric=T, mandatory=F, description="Number of core(s) to be used for computations (if available).", default=detectCores());
paramsDefinition[["--referenceAnnotationsFilename|-R"]]=list(variableName="referenceAnnotationsFilename", numeric=F, mandatory=F, description="Path to one file containing the genes lists to be used as reference for comparison (the total set). If set, output will be suffixed by '_filteredRef'. Set as 'NULL' to ignore and use all annotations found in IOF object(s).", default="NULL");
paramsDefinition[["--comparisonAnnotationsFilenames|-g"]]=list(variableName="comparisonAnnotationsFilenames", numeric=F, mandatory=F, description="Path to one or more file(s) containing the genes lists to be compared to the rest of the annotations. Set as 'NULL' to ignore.", default="NULL");
paramsDefinition[["--normalizeAgainstRef"]]=list(variableName="normalizeAgainstRef", numeric=F, mandatory=F, description="If a list of genes is provided for comparison with other annotations (see --comparisonAnnotationsFilenames|-g), defines if a normalization factor should be computed based on the remaining annotations (after eventual filtration, see --referenceAnnotationsFilename|-R). Valid values are 'NULL' to ignore normalization process, 'body' or 'TSS' to respectively match the average values in the 50%-75% range of the gene body, or the maximum value around the TSS. Ignored if no external annotation list provided.", default="body");

# Get the parameters from command line
getParams(paramsDefinition);

###


library(RFutils);

# For debugging : setwd("/sc/orga/projects/vanbah01b/top1-paper/2015-07-22_Top1-ChIP-Seq/mapping/campo/A549"); scoreFiles=c("./PR8WT_A549_RNAPII-01_06hpi_campothecin/2_bamFiles/wigsPasha_SE/AllReads/WIGvs_PR8WT_A549_RNAPII-01_06hpi_campothecin_unireads_elManual290_AThr1.wig", "PR8WT_A549_RNAPII-01_06hpi_DMSO/2_bamFiles/wigsPasha_SE/AllReads/WIGvs_PR8WT_A549_RNAPII-01_06hpi_DMSO_unireads_elManual290_AThr1.wig"); annotationFile="/sc/orga/projects/vanbah01a/reference-databases/homo-sapiens/UCSC/hg19/Annotation/RefseqGenes/refseq_GRCh37_HG19_09_2015.bed"; folderOutput="./manual/output"; folderIOF="./analyses/IOFs"; outputFormat="pdf"; namesExperiments=NULL; outputPrefix="test"; extendRegionFigure=2000; extendRegionObject=10000; rescaleData=TRUE; saveIOF=TRUE; useIOF=TRUE; mergeMultiProfiles=TRUE; nbPoints=1000; ratioInsideExtend=4; figuresHeight=10; figuresRatio=4/3; composite=TRUE; fivePrime=TRUE; threePrime=TRUE; scalingAll=TRUE; loadAnnotationsFunction=import.bed; nbCores=10; comparisonAnnotationsFilenames="/sc/orga/projects/vanbah01b/top1-paper/2015-07-22_Top1-ChIP-Seq/gene-lists/list-earlyup.ids"; referenceAnnotationsFilename="/sc/orga/projects/vanbah01b/top1-paper/2015-07-22_Top1-ChIP-Seq/gene-lists/list-active.ids"; smoothingSpan=20; 


########### I/O and arguments checks

# Check existence of input files
if(any(!file.exists(scoreFiles))) stop("At least one of the score file(s) could not be found...");
if(any(!file.exists(annotationFile))) stop("At least one of the annotations file(s) could not be found...");


# Define the name of experiments
if(is.null(namesExperiments))
{
    namesExperiments=gsub("\\..*", "", basename(scoreFiles));
} else if(length(namesExperiments)!=length(scoreFiles)) 
{
    stop("Argument '--namesExperiments|-n' must be provide as much elements as '--scoreFiles|-W' or 'NULL' to use base file names as experiment names in the results...");
}

# Remove special characters from the name of experiments
namesExperiments=gsub("[ \\.\\|\\/\\]", "_", namesExperiments);
names(scoreFiles)=namesExperiments;


# Check simple options
if(!((length(extendRegionFigure)==1) && (extendRegionFigure>0) && is.finite(extendRegionFigure))) stop("Argument '--extendRegionFigure|-e' must be a single strictly positive and finite numeric...");
if(!((length(extendRegionObject)==1) && (extendRegionObject>0) && is.finite(extendRegionObject))) stop("Argument '--extendRegionObject|-E' must be a single strictly positive and finite numeric...");
if(!((length(nbPoints)==1) && (nbPoints>0) && is.finite(nbPoints))) stop("Argument '--nbPoints|-p' must be a single strictly positive and finite numeric...");
if(!((length(ratioInsideExtend)==1) && (ratioInsideExtend>0) && is.finite(ratioInsideExtend))) stop("Argument '--ratioInsideExtend|-I' must be a single strictly positive and finite numeric...");
if(!((length(figuresHeight)==1) && (figuresHeight>0) && is.finite(figuresHeight))) stop("Argument '--figuresHeight|-h' must be a single strictly positive and finite numeric...");
if(!((length(figuresRatio)==1) && (figuresRatio>0) && is.finite(figuresRatio))) stop("Argument '--figuresRatio|-w' must be a single strictly positive and finite numeric...");
if(!((length(outputFormat)==1) && (outputFormat %in% c("png", "pdf")))) stop("Argument '--outputFormat|-f' must be a single element with value 'png' or 'pdf'...");
if(!(is.null(smoothingSpan) || ((smoothingSpan >1) && is.finite(smoothingSpan)))) stop("Argument 'smoothingSpan' must be NULL of or a finit numeric >1...");
if(!(is.null(normalizeAgainstRef) || (normalizeAgainstRef %in% c("TSS", "body"))))
{
	warning("Unrecognized value for argument 'normalizeAgainstRef'. Valid values are 'NULL', 'body', 'TSS'. Skipping normalization...");
	normalizeAgainstRef=NULL;
}


# Add a separator to output prefix if set
if(nchar(outputPrefix)>0) outputPrefix=paste("_", outputPrefix, sep="");


# Define the number of cores to be used by 'parallel' functions (might cause an error if the number of ALLOWED CORES is different from the total number of cores on the system)
if(!((length(nbCores)==1) && (((nbCores>0) && is.finite(nbCores)) || (is.na(nbCores))))) stop("Argument '--nbCores|-C' must be a single strictly positive and finite numeric or NA...");
options(mc.cores=if(is.na(nbCores)) 1 else nbCores);


# Try to create output folders
if(!(length(folderOutput)==1)) stop("Argument 'folderOutput' must provide a single value...")
outputInfo=file.info(folderOutput);
if(is.na(outputInfo["isdir"])) dir.create(folderOutput, recursive=T) else if(!outputInfo["isdir"]) stop(paste("A file (not a folder) with the name '", outputFolder,"' already exist, cannot create output folder...", sep=""));

if(!(length(folderIOF)==1)) stop("Argument 'folderIOF' must provide a single value...")
outputInfo=file.info(folderIOF);
if(is.na(outputInfo["isdir"])) dir.create(folderIOF, recursive=T) else if(!outputInfo["isdir"]) stop(paste("A file (not a folder) with the name '", folderIOF,"' already exist, cannot create IOF folder...", sep=""));



########### IOF loading or computation

# Read annotations names
cat("\nReading annotations file... ");
annotationData=loadAnnotationsFunction(annotationFile);
cat("Done.", length(annotationData), "found !");


# Predefine the IOF filenames and paths to be detected with a standard format containing relevant information
filenamesIOF=file.path(folderIOF, paste(namesExperiments, "_ext_", extendRegionObject, "_n_", length(annotationData), if(rescaleData) "_rescaled" else NULL, ".RData", sep=""));


# Read the wig file and create the objects from annotations and scores
scoresObjectList=mcmapply(function(currentScoreFile, currentExpName, currentPathIOF)
        {
            scoresObject=NULL;
            
            if(useIOF && file.exists(currentPathIOF))
            {
                cat("\nLoading scores from precomputed IOF RData file for : ", currentExpName, "... ", sep="");
                # Load the scores object from file in a temporary environment and import it
                tempEnv=new.env();
                load(currentPathIOF, envir=tempEnv);
                scoresObject=tempEnv[["scoresObject"]];
                if(is.null(scoresObject)) cat("\nThe 'scoresObject' variable could not be found in the loaded RData file, recomputing the intermediary object file...");
            }
            
            if(is.null(scoresObject))
            {
                cat("\nReading WIGvs score file for : ", currentExpName, "... ", sep="");
                wigData=readWigVs(currentScoreFile);
                if(rescaleData) wigData=rescaleWigVsData(wigData);
                
                
                # Check consistency between WIG files and annotations
                wigChromosomes=sapply(lapply(wigData, seqnames), levels);
                annotationsChromosomes=levels(seqnames(annotationData));
                commonChromosomes=intersect(wigChromosomes, annotationsChromosomes);
                if(length(commonChromosomes)==0) stop("No common chromosome names could be found between score file and annotations...");
                
                if(!((length(wigChromosomes)==length(commonChromosomes)) && (length(annotationsChromosomes)==length(commonChromosomes))))
                {
                    cat("\nReducing WIGvs and annotation datasets to common 'seqnames' references (chromosomes)... ");
                    wigData=wigData[wigChromosomes %in% commonChromosomes];
                    seqlevels(annotationData, force=TRUE)=commonChromosomes;
                    cat("Done !", length(annotationData), " annotations remaining on chromosomes : ", paste(commonChromosomes, collapse=" - "), sep="");
                }
                
                
                cat("\nExtracting scores for : ", currentExpName, "... ", sep="");
                scoresObject=AP_extractScoresFromWIGvs(wigData=wigData, annotationData=annotationData, extendSize=extendRegionObject);
                
                if(saveIOF)
                {
                    cat("Saving IOF RData file for : ", currentExpName, "... ", sep="");
                    save(scoresObject, file=currentPathIOF);
                }
            }
            return(scoresObject);
        },
        as.list(scoreFiles),
        as.list(namesExperiments),
        as.list(filenamesIOF),
        SIMPLIFY=FALSE);



########### Plotting


###  Eventually reduce the reference annotations in IOFs (total list contained in object) to a set of interesting ones
if(!is.null(referenceAnnotationsFilename))
{
	if(file.exists(referenceAnnotationsFilename))
	{
		cat("\nReducing annotation reference set according to '--referenceAnnotationsFilename|-R' argument... ");
		referenceAnnotations=gsub(".*\\|","",readLines(referenceAnnotationsFilename));
		# Reduce the total list of annotation to this list of active genes
		scoresObjectList=lapply(scoresObjectList, function(x){return(x[mcols(x)[["name"]] %in% referenceAnnotations])});
		cat("Done ! ");
		outputPrefix=paste(outputPrefix, "_filteredRef", sep="");
	} else
	{
		warning("Could not read genes list for filtering annotations reference set, skipping... ");
	}
		if(any(sapply(scoresObjectList, length)==0)) stop ("At least one sample does not provide any remaining annotation after filtering, please check IDs concordance between reference list of genes and IOF objects...");
		cat("Remaining annotations :", paste(sapply(scoresObjectList, length), collapse=" - "));
}


# Update the output name in case profiles are to be smoothed or not
if(!is.null(smoothingSpan)) outputPrefix=paste(outputPrefix, "_smoothed", smoothingSpan, sep="");



### Comparison of a list of annotation with reference

# If a list of annotations is provided for comparison with the other ones present in the IOFs, make separate sublists by divinding IOFs according to this list, eventually normalize based on the annotations in the remaining annotations, and plot them on the same graph (unlisting at first level), or on a separate graph for each IOF (mergeMultiProfiles).
if(!is.null(comparisonAnnotationsFilenames))
{

	for(currentComparisonAnnotationsFilename in comparisonAnnotationsFilenames)
	{

		## Split IOFs in two sublists according to annotations list provided

		# Define a name for the list of annotations to be compared to reference based on its filename (without extension)
		comparisonAnnotationsListName=gsub("\\..*", "", basename(currentComparisonAnnotationsFilename));
		cat("\n\nUsing genes list :", comparisonAnnotationsListName);

		# Reading list of annotations and isolate identifiers (second part after a '|', and remove suffix after '.')
		comparisonAnnotationsList=gsub("\\..*","",gsub(".*\\|","",readLines(currentComparisonAnnotationsFilename)));

		# Check that some annotations of the list were found in the scores object
		annotationsInListInd=(mcols(scoresObjectList[[1]])[["name"]] %in% comparisonAnnotationsList)
		if(!any(annotationsInListInd)) stop("Not any genes in the list to compared would be found in the reference set (total annotations after eventual filtering on reference genes list)...");

		# Add a nested list layer to the scores list, split each IOFs and do a group with and a group without the annotations in comparisonAnnotationsList
		scoresObjectListOfList=lapply(scoresObjectList, function(x){listReturn=list(); listReturn[[comparisonAnnotationsListName]]=x[annotationsInListInd]; listReturn[["others"]]=x[!annotationsInListInd]; return(listReturn)});



		## Normalize each sample curves according to the reference annotation ('others') for each IOF

		if(!is.null(normalizeAgainstRef))
		{
			cat("\nNormalizing scores of genes list according to the other remaining annotations... ");

			ratiosNormalise=NULL;

			# Extract the average or max scores at TSS or gene body from reference annotation ('others') for each IOF.
			# Do this by computing the (eventually smoothed) AP from interpolated matrices as done internally during plotting.
			# Could have been done on non-interpolated matrice using 'AP_getScoresROI' but could have led to inconsistent GRAPHICAL results in case of small 'nbPoints' values or smoothing.
			if(normalizeAgainstRef=="body")
			{
				# Extract the average value between 50% and 75% of annotation body for each reference list
				ratiosNormalise=mclapply(scoresObjectListOfList, function(x)
									{ 
										matrixObjectReference_composite=AP_buildMatrixFromObject(x[["others"]], extendSize=extendRegionFigure, nbPoints=nbPoints, ratioInsideExtend=ratioInsideExtend);
										startIND=(matrixObjectReference_composite$nbPointsExtend+(matrixObjectReference_composite$nbPointsInside*0.5));
										endIND=(matrixObjectReference_composite$nbPointsExtend+(matrixObjectReference_composite$nbPointsInside*0.75));
										AP=colMeans(matrixObjectReference_composite$profileMatrix)[startIND:endIND]
										return(mean(if(is.null(smoothingSpan)) AP else rollApply(AP, smoothingSpan, mean), na.rm=TRUE));
									});
		
			} else if(normalizeAgainstRef=="TSS")
			{
				# Extract the max value around TSS for each reference list of annotations
				ratiosNormalise=mclapply(scoresObjectListOfList, function(x)
									{
										focusedTSS=flank(x[["others"]], width=-1, start=TRUE);
										matrixObjectReference_TSS=AP_buildMatrixFromObject(focusedTSS, extendSize=extendRegionFigure, nbPoints=nbPoints, ratioInsideExtend=ratioInsideExtend);
										AP=colMeans(matrixObjectReference_TSS$profileMatrix)
										return(max(if(is.null(smoothingSpan)) AP else rollApply(AP, smoothingSpan, mean), na.rm=TRUE));
									});				
			} 

			# Write the normalization factors to file
			writeLines(paste(names(ratiosNormalise), unlist(ratiosNormalise), sep=" "), con=file.path(folderOutput, paste(comparisonAnnotationsListName,"_normalizationFactors_", normalizeAgainstRef, outputPrefix, ".txt", sep="")));

			# Go trhough the IOF objects of each sample and divide the scores by the corresponding normalization factor
			scoresObjectListOfList=Map(lapply, scoresObjectListOfList, ratiosNormalise, MoreArgs=list(FUN=function(x,normFactor){mcols(x)[["scoresRle"]]=lapply(mcols(x)[["scoresRle"]], "/", normFactor); return(x);}));
			 
			cat("Done !");
		}



		## Plot the profiles. Either all on the same graph by unlisting the first level of IOF list or on separate files by looping on this first level.
		# In any case the 'mergeMultiProfiles' argument of AP function must be true as we plot two profiles per IOF (list of annotations and reference).
		if(mergeMultiProfiles)
		{

			if(any(c(composite, fivePrime, threePrime)))
			{
				cat("\nPlotting merged normalized average profiles... ");

			# We merge all samples and sublist of genes for plotting (unlist) but we still want to keep track of sample groups for coloring (and rescaling)
			samplesGroups=factor(rep(names(scoresObjectListOfList), each=2));

			returnValueAP=AP_averageProfile(GRangesList(unlist(scoresObjectListOfList, recursive=FALSE)), 
							    outputFile_baseName=file.path(folderOutput, paste(comparisonAnnotationsListName,"_ALL", outputPrefix, if(is.null(normalizeAgainstRef)) NULL else paste("_norm", normalizeAgainstRef, sep=""), sep="")), 
							    extendSize=extendRegionFigure, 
							    ratioInsideExtend=ratioInsideExtend, 
							    nbPoints=nbPoints, 
							    outputFormat=outputFormat, 
							    mergeMultiProfiles=TRUE, 
							    plotProfile=c(composite=composite, fivePrime=fivePrime, threePrime=threePrime), 
							    scaling=if(scalingAll) "all" else NULL, 
							    main="Merged Profile",
							    suffix=c(composite="Composite", fivePrime="5prime", threePrime="3prime"), 
							    fivePrimeCol="green", 
							    threePrimeCol="red", 
							    figuresHeight=figuresHeight, 
							    figuresRatio=figuresRatio,
							    noScaleExtension=FALSE,
							    col=rainbow(length(scoresObjectList))[samplesGroups],
								lty=1:2,
								plotSmooth=TRUE,
								smoothingSpan=smoothingSpan);

				cat("Done !");
			}

		} else # => !mergeMultiProfiles : each IOFs to be plotted in a separate file
		{

			# Go through the scores list to plot the individual average profile of selected annotations against the rest of annotations (2 curves)
			resNULL=Map(function(x, sampleName)
			{
				if(any(c(composite, fivePrime, threePrime)))
				{
					cat("\nPlotting average profiles for : ", sampleName, "... ", sep="");

					returnValueAP=AP_averageProfile(GRangesList(x), 
									    outputFile_baseName=file.path(folderOutput, paste(comparisonAnnotationsListName,"_", sampleName, outputPrefix, if(is.null(normalizeAgainstRef)) NULL else paste("_norm", normalizeAgainstRef, sep=""), sep="")), 
									    extendSize=extendRegionFigure, 
									    ratioInsideExtend=ratioInsideExtend, 
									    nbPoints=nbPoints, 
									    outputFormat=outputFormat, 
									    mergeMultiProfiles=TRUE, 
									    plotProfile=c(composite=composite, fivePrime=fivePrime, threePrime=threePrime), 
									    scaling=if(scalingAll) "all" else NULL, 
									    main="Average Profile", 
									    suffix=c(composite="Composite", fivePrime="5prime", threePrime="3prime"), 
									    fivePrimeCol="green", 
									    threePrimeCol="red", 
									    figuresHeight=figuresHeight, 
									    figuresRatio=figuresRatio,
									    noScaleExtension=FALSE,
									    col=if(is.null(colors)) rainbow(length(x)) else colors,
										lty=1:2,
										plotSmooth=TRUE,
										smoothingSpan=smoothingSpan);

					cat("Done !");
				}
			},
			scoresObjectListOfList, 
			as.list(names(scoresObjectListOfList)));
		}

	} # for loop on annotations lists to be compared to reference

} else # => comparisonAnnotationsFilenames==NULL : no list to be used for comparison, plot a single profile for each IOF, let the AP function eventually merge all profiles on same graph (mergeMultiProfiles)
{

				if(any(c(composite, fivePrime, threePrime)))
				{
					cat("\nPlotting average profiles... ", sep="");

					returnValueAP=AP_averageProfile(GRangesList(scoresObjectList), 
									    outputFile_baseName=file.path(folderOutput, paste("AP", outputPrefix, sep="")), 
									    extendSize=extendRegionFigure, 
									    ratioInsideExtend=ratioInsideExtend, 
									    nbPoints=nbPoints, 
									    outputFormat=outputFormat, 
									    mergeMultiProfiles=mergeMultiProfiles, 
									    plotProfile=c(composite=composite, fivePrime=fivePrime, threePrime=threePrime), 
									    scaling=if(scalingAll) "all" else NULL, 
									    main="Average Profile", 
									    suffix=c(composite="Composite", fivePrime="5prime", threePrime="3prime"), 
									    fivePrimeCol="green", 
									    threePrimeCol="red", 
									    figuresHeight=figuresHeight, 
									    figuresRatio=figuresRatio,
									    noScaleExtension=FALSE,
									    col=if(is.null(colors)) rainbow(length(scoresObjectList)) else colors,
										plotSmooth=TRUE,
										smoothingSpan=smoothingSpan);

					cat("Done !");
				}

}

