#!/usr/bin/env Rscript
#
# Script that must be started from command line. 
# Takes as argument filenames of one or several 'WIG fixed step' files.
# Sort the chromosomes by numeric and alpha values and writes the resulting
# wig data in a new 'WIG fixed step' file (with suffix added to filename).
#
# Command : sortWIG.R -w "wigFile1" "wigFile2" "wigFile3" [-b 50] [-s "_sortedChroms"]
#
# Author : R. Fenouil - 21/11/2014
#
# Dependences : R libraries (Rargs, Pasha, gtools)
#

library(Rargs)
library(Pasha)
library(gtools)


##### Get command line parameters (filenames etc...)

# Parameters format definition
paramsDefinition=list();

# mandatory (MUST BE SPECIFIED BY USER, DEFAULT VALUES ARE SPECIFIED FOR INFORMATION)
paramsDefinition[["--wigFiles|-w"]]=list(variableName="wigFiles", numeric=F, mandatory=T, description="WIG fixed step file(s) to sort", default="./example.wig");

# non-mandatory (default value applied)
paramsDefinition[["--binSize|-b"]]=list(variableName="binSize", numeric=T, mandatory=F, description="Bin size of fixed steps (recycled if necessary)", default=50);
paramsDefinition[["--suffixResultFilename|-s"]]=list(variableName="suffixResultFilename", numeric=F, mandatory=F, description="Suffix to append to original filename for resulting file (before .wig extension, recycled if necessary)", default="_sortedChroms");

# Get the parameters from command line
getParams(paramsDefinition);


##### Check arguments

if((length(binSize)>length(wigFiles)) || (length(suffixResultFilename)>length(wigFiles))) stop("Arguments '--binSize|-b' and '--suffixResultFilename|-s' MUST NOT contain more elements than argument '--wigFiles|-w'...")


##### Work

sortWIGfs=function(filename, binSize, suffixResultFilename)
{
    cat("\nSorting file : ",filename,"...\n", sep="");
    wigData=readWIG(filename);
    writeWIG(wigData[mixedorder(names(wigData))], fileName=gsub("\\.wig$", suffixResultFilename, filename, ignore.case=TRUE), fixedStep=binSize);
    cat("\nDone !");
}

# Apply sorting function to all files (eventually recycling arguments if necessary)
invisible(mapply(sortWIGfs, wigFiles, binSize, suffixResultFilename));


