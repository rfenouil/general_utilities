\name{plotGenomeTrack}
\alias{plotGenomeTrack}
%- Also NEED an '\alias' for EACH other topic documented here.
\title{Plot tracks as in genome browser}
\description{
Plot genome tracks with annotations and enrichment data, together with coordinates. Allows to focus on specific annotations, highlight genomic ranges, and offers several options for data (scores) representation (type, scale).
}
\usage{
plotGenomeTrack=(dataList=list(), fileTypes="WIG_VS", normFactors=NULL, genome="hg19", collapseTranscripts=TRUE, geneSymbol=NULL, extendRangePct=10, hideOtherAnnotations=FALSE, chromosome=NULL, from=0, to=NULL, dataPlotType="histogram", dataCol=c("orange", "pink", "lightblue", "violet"), manualScale=NULL, uniqueDataScale=FALSE, preferedMaxScale=NULL, orient5to3=NULL, highlightRanges=NULL, showChromosomeInTitle=TRUE, annotationsPos="split", returnNormalizedData=FALSE, omitCoordinatesTrack=FALSE, ...)
}
%- maybe also 'usage' for other objects documented here.
\arguments{
  \item{dataList}{A string, a GRanges objects, or a list of it. When 'dataList' is a string, it will be used as the path for the file to be loaded (valid types are WIG variable step and BAM). If a GRanges is provided, it should carry one numeric meatadata column containing the enrichment score for each range (mainly used for chaining plots on same dataset, see returning value). If specified as a list, both types could be mixed, all data tracks will be plotted in the same order of list elements. Track names will be derived from list name (Automatically numbered if not specified).}
  \item{fileTypes}{A string array. If dataList (or elements of it) is a path to a file, defines the type of file to be expected for data. Valid file types are 'BAM' and 'WIG_VS' (variable step). Recycled to match dataList if necessary.}
  \item{normFactors}{A numeric array or NULL. If set the numeric values will be used to normalize scores before plotting data tracks. A simple multiplicative operation is applied on all numeric met-columns of internal GRanges objects. By default (see 'returnNormalizedData'), preloaded GRanges objects are returned by the function BEFORE normalization.}
  \item{genome}{A string. If the string is a path to an existing file (GTF, GFF, BED, ...), it will be used for annotation tracks. If 'genome' is a valid UCSC ID for genome version (eg. HG19) it will be used to get annotations from BioMart (not implemented yet). See 'GeneRegionTrack' for valid annotation file types and UCSC genome IDs.}
  \item{collapseTranscripts}{A logical. If several transcripts are defined for genes annotations they will eventually be merged as a single 'meta-gene' composed with introns/exons of all transcripts.}
  \item{showAnnotationsId}{A logical. Determines if annotations names are drawn on the figure. Can also be overridden using '...' parameter 'showId' that affects all tracks (highlights names in geneomic coordinates tracks namely).}
  \item{geneSymbol}{A string array or NULL. If specified, the genomic coordinates of the figure will be focused on the annotation(s) matching 'geneSymbol' (overrides 'from', 'to', and 'chromosome' values). If several annotations are found, the figure will provide a range of genomic coordinates fitting all instances. However if annotations are spread on different chromosomes, an error is thrown.}
  \item{extendRangePct}{A finite numeric value. When 'geneSymbol' is set, the genomic coordinates covered by the figure is extended by 'extendRangePct' percents (in each direction) around the selected annotation(s) to allow for a visualization of surrounding regions.}
  \item{hideOtherAnnotations}{A logical. If TRUE and 'geneSymbol' is set, all other annotations will be removed from the plot.}
  \item{chromosome}{A string or NULL. Ignored if 'geneSymbol' is set. Defines which chromosome to show in the figure.}
  \item{from}{A numeric or NULL. Ignored if 'geneSymbol' is set. Defines the 5' bound of the range to show on the figure.}
  \item{to}{A numeric or NULL. Ignored if 'geneSymbol' is set. Defines the 3' bound of the range to show on the figure}
  \item{dataPlotType}{A string array. Define the style of plot for data tracks. Valid values are 'histogram' (recommended), 'heatmap', and 'line'. Recycled if necessary.}
  \item{dataCol}{A valid color array. Define the color for plotting data tracks (automatically adapts to dataPlotType). Recycled if necessary.}
  \item{manualScale}{NULL, a numeric array with 2 elements, or a list of it (if needs to be specified for individual tracks). Define the scale for data tracks (eventually recycled). By default scale for data track is defined during the plotting process, and fits the data values in the plotted genomic range.}
  \item{uniqueDataScale}{A logical. Ignored if scale is defined manually. If true, the scale of all data tracks will be made identical and designed to fit scores in all tracks.}
  \item{preferedMaxScale}{A numeric value or NULL. Ignored if scale is defined manually or if 'uniqueDataScale' is FALSE. Used to define a minimal upper-scale value when a common scale is computed for all tracks.}
  \item{orient5to3}{A logical or NULL. Determines the orientation of the final figure. If NULL the default is to plot 5' on the left and 3' on the right (as in regular genome browsers). A FALSE value will reorient the figure and plot 3' on the left and 5' on the right. When a 'geneSymbol' is specified, the figure will eventually be reoriented to match annotation orientation (depending on its strand). This latter behavior is ignored (with a warning) if genes selected by 'geneSymbol' are spread on different strands.}
  \item{highlightRanges}{An IRanges object or NULL. When set, the specified ranges will be highlighted in the coordinates track (in between the 5' to 3' and 3' to 5' genomic coordinates arrows). If the IRanges carry an additional metacolumn (named 'names') with a value for each range, these names will be overlaid on the highlighted ranges.}
  \item{showChromosomeInTitle}{A logical. By default (TRUE), the chromosome name is plotted on the left of the genomic coordinates track (in the tracks title area). However this option make possible to relocate the chromosome name in between the 5' to 3' and 3' to 5' arrows. It will however be ignored (and chromosome name put back in track title area) if some ranges needs to be highlighted (see 'highlightRanges').}
  \item{annotationsPos}{A scalar string. Defines the position of annotations tracks relative to the genomic coordinates track. valid values are 'split', 'above', and 'below'. When value is 'split', annotations are separated in two tracks plotted on both sides of genomic coordinates (positive strand above and negative below). 'above' and 'below' will group all annotations in a single track and place them above or below the genomic coordinates track respectively.}
  \item{returnNormalizedData}{A logical. If 'normFactors' are specified, determines if GRanges objects returned after plotting should be the normalized or not (just loaded data). Default value returns non-normalized data so the function calls can be chained with identical parameters (including 'normFactors') and normalization factors will not be re-applied on already normalized values.}
  \item{omitCoordinatesTrack}{A logical. If TRUE, the track with genomic coordinates will not be included in the figure.}
  \item{...}{All remaining arguments will be passed to 'plotTracks' call for plotting (except 'trackList', 'chromosome', 'from', 'to', 'collapseTranscripts', 'transcriptAnnotation', 'reverseStrand' which are determined internally and already provided during the function call). This is useful to redefine graphic parameters for all tracks simultaneously (when applicable, see displayPars, GeneRegionTrack, GenomeAxisTrack, DataTrack, and plotTracks documentations pages for relevant arguments).}
}
\details{
This function uses primarily GViz to plot genomic coordinates, annotations, and 
enrichment scores tracks.
It provides a convenient way to format and organize data, namely several options 
to manage the scale of data tracks.

In addition to BAM files for which the coverage is automatically computed, it
allows to load data scores from WIG variable step files that is not supported
by 'Gviz' nor 'Rtracklayer' natively.

Internally, the function will load the data (scores) files and keep them stored
as GRanges. Conversion to DataTrack objects is made right before plotting as
some data modifications have to be made on datasets depending on the type of 
plot attempted (scores modifications are easier on GRanges).

This list of GRanges is returned by the function after plotting and can directly 
be reused as input in 'dataList' argument. This allows to save the long time 
required for parsing large files (WIG specially as BAM implement streaming). As
'dataList' can mix objects and filenames, it is possible to add new filenames in 
this list before re-injecting it in a later call. Note that by default GRanges
objects are returned before normlization process (see 'returnNormalizedData').

Compatibility with GFF/GTF annotations format:
Gviz seems able to plot with ease UCSC GTF files but has some trouble with 
formats variation like GTF downloaded from ensembl or gencode which carry more 
information (genes symbol namely). 
The parsing seems to work as genes/symbols/transcripts are properly reported in 
DataTrack objects but it cannot resolve internal annotations structures during
plotting (plots a box instead of exons etc...).
A workaround for being able to use genes symbols directly is either to integrate 
this information in UCSC downloaded files or to download the refflat (from UCSC 
table browser) which contain genes symbols for refseq annotations. 

NOTE : in its current version, GViz does not support to plot annotations IDs 
above or below annotation (eg. just.group='above') when coordinates are oriented 
from 3' to 5' (an error is raised).
}
\value{
A list of GRanges defining scores on genomic positions as defined in loaded data 
files (see 'returnNormalizedData').
}
\author{Romain Fenouil}
\seealso{
  \code{\link[rtracklayer]{import.gff}}
  \code{\link[Gviz]{displayPars}}
  \code{\link[Gviz]{GeneRegionTrack}}
  \code{\link[Gviz]{GenomeAxisTrack}}
  \code{\link[Gviz]{DataTracks}}
  \code{\link[Gviz]{plotTrackse}}
  \code{\link[RFutils]{readWigVs}}
}
\examples{
\dontrun{

######## Locate annotations and data example files


# Annotation file (Human transcripts chr21-22)
annotationsFileName="refseq_refflat_UCSC_HG19_chr21-22.gtf.gz";
annotationsFilePath=system.file("extdata", "AP", annotationsFileName, package="RFutils");

# WIG variable step data file (Human RNA Pol-II chr21-22)
wigFileName="WIGvs_PolII-N20_chr21-22.wig.gz";
wigFilePath=system.file("extdata", "AP", wigFileName, package="RFutils");


######## Plot figures with annotations and scores tracks 

preloadedData=plotGenomeTrack(dataList=list("Pol.II - N20"=wigFilePath), fileTypes="WIG_VS", genome=annotationsFilePath, chromosome="chr22", from=20000000, to=25000000);

# Reuse the preloaded data (faster), and focus on one gene of interest (extend 
# 6 times the gene size on each side and reorient for negative stranded gene)
plotGenomeTrack(dataList=preloadedData, genome=annotationsFilePath, geneSymbol="PPM1F", extendRangePct=600, orient5to3=TRUE, hideOtherAnnotations=TRUE, annotationsPos="above");

# Repeat the preloaded data (for example purpose) and play with graphic 
# parameters for tracks titles and chromosome name
plotGenomeTrack(dataList=list("uno"=preloadedData[[1]], "dos"=preloadedData[[1]], "tres"=preloadedData[[1]]), genome=annotationsFilePath, chromosome="chr22", from=23000000, to=24000000, dataPlotType=c("histogram", "line", "heatmap"), background.title="transparent", col.title="darkgrey", col.axis="red", showChromosomeInTitle=FALSE);


}
}